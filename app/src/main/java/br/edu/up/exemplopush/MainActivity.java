package br.edu.up.exemplopush;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONObject;

import java.util.Set;

public class MainActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    TextView txt = (TextView) findViewById(R.id.txtMensagens);
    Bundle bundle = getIntent().getExtras();

    if (bundle != null){
      Set<String> keyset = bundle.keySet();
      for (String key : keyset) {
        String str = bundle.getString(key);
        txt.append("\n" + key + ": " + str);
      }
    }
  }

  public void onClickEnviar(View view) {

    EditText txt = (EditText) findViewById(R.id.editText);
    String msg = txt.getText().toString();

    try {
      HttpClient client = HttpClientBuilder.create().build();
      HttpPost post = new HttpPost("https://fcm.googleapis.com/fcm/send");
      post.setHeader("Content-type", "application/json");
      post.setHeader("Authorization", "key=AIzaSyBSheHssXevRq0trDbA9mhnY_2jqMoeChA");

      JSONObject message = new JSONObject();
      message.put("to", "dBbB2BFT-VY:APA91bHrvgfXbZa-K5eg9vVdUkIsHbMCwHRVc8dBAvoH_3ZtaahVVeMXP7Bm0iera5s37ChHmAVh29P8aAVa8HF0I0goZKPYdGT6lNl4MXN0na7xbmvF25c4ZLl0JkCDm_saXb51Vrte");
      message.put("priority", "high");

      JSONObject notification = new JSONObject();
      notification.put("title", "Java");
      notification.put("body", msg);
      message.put("notification", notification);

      post.setEntity(new StringEntity(message.toString(), "UTF-8"));
      client.execute(post);
      Toast.makeText(this, "Mensagem enviada!", Toast.LENGTH_SHORT).show();

    } catch (Exception e) {
      Toast.makeText(this, "Algo deu errado...", Toast.LENGTH_SHORT).show();
      e.printStackTrace();
    }
  }
}
