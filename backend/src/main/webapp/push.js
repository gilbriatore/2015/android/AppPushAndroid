$("#button").click(function(){

  var mensagem = $("#mensagem").val();
  $("#mensagem").val("");

  var json = {
    //Dispositivo de destino (ver FirebaseInstanceIdService).
    "to": "dBbB2BFT-VY:APA91bHrvgfXbZa-K5eg9vVdUkIsHbMCwHRVc8dBAvoH_3ZtaahVVeMXP7Bm0iera5s37ChHmAVh29P8aAVa8HF0I0goZKPYdGT6lNl4MXN0na7xbmvF25c4ZLl0JkCDm_saXb51Vrte",
    "notification": {
       "title": "Push enviado com JQuery",
       "body": mensagem
    },
    "data": {
       "title": "Título de dados",
       "body": "corpo de dados...."
    }
  };

  $.ajax({
    //Url do Firebase para envio de mensagens push.
    url: 'https://fcm.googleapis.com/fcm/send',
    type: "POST",
    processData : false,
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Content-Type', 'application/json');
       //Server key do Cloud Messaging (ver Firebase Console).
      xhr.setRequestHeader('Authorization', 'key=AIzaSyBSheHssXevRq0trDbA9mhnY_2jqMoeChA');
    },
    data: JSON.stringify(json),
    success: function () {
     console.log("Mensagem enviada com sucesso!");
    },
    error: function(error) {
     console.log(error);
    }
  });
});